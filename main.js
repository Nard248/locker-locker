ymaps.ready(init);


function init() {
    var map = new ymaps.Map('map',
        {
            center: [40.177623, 44.512520],
            zoom: 12,
            controls: ['zoomControl']
        });


    // window.onload = function () {
    //     window.onscroll = function () {
    //         var doc = document.body,
    //             scrollPosition = doc.scrollTop,
    //             pageSize = (doc.scrollHeight - doc.clientHeight),
    //             percentageScrolled = Math.floor((scrollPosition / pageSize) * 100);
    //
    //         if (percentageScrolled >= 50){ // if the percentage is >= 50, scroll to top
    //             window.scrollTo(0,0);
    //         }
    //     };
    // };
    // // setInterval(()=> {
    //     console.log(screen.pixelDepth)
    // // }, 1500)
    //
    // // document.addEventListener('change', function () {
    // //     if ()
    // // })

    let kentronObjects = [
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.178202, 44.515583]
                },
                properties: {
                    clusterCaption: 'Geo object 1',
                    hintContent: "2.0",
                    balloonContentBody: "Հրապարակ"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.187217, 44.521165]
                },
                properties: {
                    clusterCaption: 'Geo object 7',
                    hintContent: "8.0",
                    balloonContentBody: "Երիտասարդական"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.179660, 44.488106]
                },
                properties: {
                    clusterCaption: 'Geo object 9',
                    hintContent: "10.0",
                    balloonContentBody: `<div class="locker-popup-wrapper">
    <div class="locker-popup-header">
        <div class="locker-popup-title-wrapper">
            <h2 class="locker-popup-title">ԴԱԼՄԱ ԳԱՐԴԵՆ ՄՈԼ</h2>
        </div>
        <img src="img/locker.png" alt="locker" class="locker-img">
    </div>
    <div class="locker-body-wrapper">
        <p class="locker-body-title">
            <strong>
                ՀԱՍՑԵ
            </strong>
        </p>
        <span class="locker-body-subtitle">
            ք. Երևան, Ծիծեռնակաբերդի խճուղի 3
        </span>
        <span class="locker-body-subtitle">
        SMART Locker-ը տեղադրված է Դալմա Գարդեն Մոլի -1 հարկում, անմիջապես «Jysk» խանութ սրահի հարևանությամբ:
        </span>

        <p class="locker-body-title">
            <strong>
                ԱՇԽԱՏԱՆՔԱՅԻՆ ԺԱՄԵՐ
            </strong>
        </p>
        <span class="locker-body-subtitle">
            Երկուշաբթի - Կիրակի, 10։00 - 22։00
        </span>
    </div>
</div>`
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.178690, 44.503905]
                },
                properties: {
                    clusterCaption: 'Geo object 12',
                    hintContent: "13.0",
                    balloonContentBody: "YC Պրոսպեկտ"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.191635, 44.505962]
                },
                properties: {
                    clusterCaption: 'Geo object 17',
                    hintContent: "18.0",
                    balloonContentBody: "Մարշալ Բաղրամյան"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.171563, 44.513027]
                },
                properties: {
                    clusterCaption: 'Geo object 18',
                    hintContent: "19.0",
                    balloonContentBody: "Զորավար Անդրանիկ"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.181817, 44.517243]
                },
                properties: {
                    clusterCaption: 'Geo object 19',
                    hintContent: "20.0",
                    balloonContentBody: "Մոսկվա"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.176024, 44.512175]
                },
                properties: {
                    clusterCaption: 'Geo object 25',
                    hintContent: "26.0",
                    balloonContentBody: "Էրեբունի Պլազա"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.184941, 44.512102]
                },
                properties: {
                    clusterCaption: 'Geo object 26',
                    hintContent: "27.0",
                    balloonContentBody: "Alpha Pharm - №072 Մաշտոցի 31/10"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.163944, 44.516421]
                },
                properties: {
                    clusterCaption: 'Geo object 32',
                    hintContent: "33.0",
                    balloonContentBody: "Alpha Pharm - №042 Խորենացու 47շ, 191 "
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.169773, 44.489127]
                },
                properties: {
                    clusterCaption: 'Geo object 46',
                    hintContent: "47.0",
                    balloonContentBody: "Digitain"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
    ]

    let malatiaObjects = [
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.173447, 44.445617]
                },
                properties: {
                    clusterCaption: 'Geo object 23',
                    hintContent: "24.0",
                    balloonContentBody: "Alpha Pharm - №050 Րաֆֆու 39/10"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.180550, 44.445685]
                },
                properties: {
                    clusterCaption: 'Geo object 24',
                    hintContent: "25.0",
                    balloonContentBody: "Alpha Pharm - №130 Օհանովի 9/1"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
    ]

    let ajapnyakObjects = [
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.194733, 44.473539]
                },
                properties: {
                    clusterCaption: 'Geo object 27',
                    hintContent: "28.0",
                    balloonContentBody: "Alpha Pharm - №004 Շին․ փող, շուկա"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.209779, 44.464127]
                },
                properties: {
                    clusterCaption: 'Geo object 35',
                    hintContent: "36.0",
                    balloonContentBody: "Garage Mall"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.196589, 44.479848]
                },
                properties: {
                    clusterCaption: 'Geo object 36',
                    hintContent: "37.0",
                    balloonContentBody: "Թումո"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
    ]

    let shengavitObjects = [
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.155531, 44.499337]
                },
                properties: {
                    clusterCaption: 'Geo object 2',
                    hintContent: "3.0",
                    balloonContentBody: "Yerevan Mall"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.154995, 44.478917]
                },
                properties: {
                    clusterCaption: 'Geo object 6',
                    hintContent: "7.0",
                    balloonContentBody: "YC Ծերեթելի"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.150756, 44.483430]
                },
                properties: {
                    clusterCaption: 'Geo object 16',
                    hintContent: "17.0",
                    balloonContentBody: "Գարեգին Նժդեհ"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
    ]

    let norkObjects = [
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.197273, 44.567135]
                },
                properties: {
                    clusterCaption: 'Geo object 0',
                    hintContent: "1.0",
                    balloonContentBody: "Mega Mall"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.204332, 44.565515]
                },
                properties: {
                    clusterCaption: 'Geo object 31',
                    hintContent: "32.0",
                    balloonContentBody: "Alpha Pharm - №051 Մոլդովական 4/13ա"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.175304, 44.566175]
                },
                properties: {
                    clusterCaption: 'Geo object 33',
                    hintContent: "34.0",
                    balloonContentBody: "Alpha Pharm - №008 Ջ. Կարախանյան 51"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
    ]

    let zeytunObjects = [
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.202528, 44.545442]
                },
                properties: {
                    clusterCaption: 'Geo object 14',
                    hintContent: "15.0",
                    balloonContentBody: "YC Զեյթուն"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.202100, 44.522941]
                },
                properties: {
                    clusterCaption: 'Geo object 29',
                    hintContent: "30.0",
                    balloonContentBody: "Alpha Pharm - №159 Ազատության 10"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
    ]

    let avanObjects = [
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.218828, 44.550078]
                },
                properties: {
                    clusterCaption: 'Geo object 13',
                    hintContent: "14.0",
                    balloonContentBody: "Ծարավ աղբյուր"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.217852, 44.581270]
                },
                properties: {
                    clusterCaption: 'Geo object 15',
                    hintContent: "16.0",
                    balloonContentBody: "YC Ավան"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.221549, 44.573094]
                },
                properties: {
                    clusterCaption: 'Geo object 30',
                    hintContent: "31.0",
                    balloonContentBody: "Alpha Pharm - №075 Ավան-Առինջ, Դուշանբե փող. 4/1 / Աշխաբադ 2/9"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
    ]

    let arabkirObjects = [
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.197638, 44.493645]
                },
                properties: {
                    clusterCaption: 'Geo object 4',
                    hintContent: "5.0",
                    balloonContentBody: "Բարեկամություն"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.205975, 44.521760]
                },
                properties: {
                    clusterCaption: 'Geo object 5',
                    hintContent: "6.0",
                    balloonContentBody: "YC Կոմիտաս"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.199826, 44.491319]
                },
                properties: {
                    clusterCaption: 'Geo object 10',
                    hintContent: "11.0",
                    balloonContentBody: "Մերգելյան"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.204379, 44.501901]
                },
                properties: {
                    clusterCaption: 'Geo object 20',
                    hintContent: "21.0",
                    balloonContentBody: "Alpha Pharm - №079 Վ.Փափազյան 21"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.194186, 44.487285]
                },
                properties: {
                    clusterCaption: 'Geo object 21',
                    hintContent: "22.0",
                    balloonContentBody: "Alpha Pharm - №078 Կիևյան 10-76"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.206656, 44.511012]
                },
                properties: {
                    clusterCaption: 'Geo object 22',
                    hintContent: "23.0",
                    balloonContentBody: "Alpha Pharm - №096 Կոմիտաս 46"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.212449, 44.523323]
                },
                properties: {
                    clusterCaption: 'Geo object 34',
                    hintContent: "35.0",
                    balloonContentBody: "Երազ։ 1001"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
    ]

    let davtashenObjects = [
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.220454, 44.493000]
                },
                properties: {
                    clusterCaption: 'Geo object 8',
                    hintContent: "9.0",
                    balloonContentBody: "YC Դավիթաշեն"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
    ]

    let erebuniObjects = [
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.128881, 44.524710]
                },
                properties: {
                    clusterCaption: 'Geo object 28',
                    hintContent: "29.0",
                    balloonContentBody: "Alpha Pharm - №012 Խաղաղ Դոնի 16/5"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
    ]

    let marashObjects = [
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.179985, 44.538759]
                },
                properties: {
                    clusterCaption: 'Geo object 3',
                    hintContent: "4.0",
                    balloonContentBody: "BetConstruct"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.190561, 44.532309]
                },
                properties: {
                    clusterCaption: 'Geo object 11',
                    hintContent: "12.0",
                    balloonContentBody: "Synergy"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            })
    ]

    let ejmiacinObjects = [
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.163322, 44.292576]
                },
                properties: {
                    clusterCaption: 'Geo object 43',
                    hintContent: "44.0",
                    balloonContentBody: "Alpha Pharm  8/1 Մովսես Խորենացու փողոց,"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
    ]

    let armavirObjects = [
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.145404, 44.034644]
                },
                properties: {
                    clusterCaption: 'Geo object 42',
                    hintContent: "43.0",
                    balloonContentBody: "ԻՍՍՈ Մոլ"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
    ]

    let gyumriObjects = [
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.793438, 43.843212]
                },
                properties: {
                    clusterCaption: 'Geo object 37',
                    hintContent: "38.0",
                    balloonContentBody: "Supermarket Carona"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.786733, 43.841246]
                },
                properties: {
                    clusterCaption: 'Geo object 38',
                    hintContent: "39.0",
                    balloonContentBody: "Alpha Pharm - Աբովյան փողոց 135"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.828957, 43.829414]
                },
                properties: {
                    clusterCaption: 'Geo object 39',
                    hintContent: "40.0",
                    balloonContentBody: "Alpha Pharm - Պ. Սևակ 9/2 (58 թաղամաս)"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
    ]

    let vanadzorObjects = [
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.810791, 44.487070]
                },
                properties: {
                    clusterCaption: 'Geo object 40',
                    hintContent: "41.0",
                    balloonContentBody: "Alpha Pharm - Տիգրան Մեծ 47/11"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.803829, 44.515502]
                },
                properties: {
                    clusterCaption: 'Geo object 41',
                    hintContent: "42.0",
                    balloonContentBody: "Supermarket Livar"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
    ]

    let gavarObjects = [
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.350923, 45.125179]
                },
                properties: {
                    clusterCaption: 'Geo object 45',
                    hintContent: "46.0",
                    balloonContentBody: "Գավառ ԱԳԱԹ Բոշնաղյան 4"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
    ]

    let martuniObjects = [
        new ymaps.GeoObject(
            {
                geometry: {
                    type: 'Point',
                    coordinates: [40.139753, 45.301490]
                },
                properties: {
                    clusterCaption: 'Geo object 44',
                    hintContent: "45.0",
                    balloonContentBody: "Մարտունի Սիթի, Մյասնիկյան 24"
                },
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/onex.png',
                iconImageSize: [36, 57]
            }),
    ]

    const clusterIcons = [
        {
            href: 'img/onex.png',
            size: [40, 60],
            // Отступ, чтобы центр картинки совпадал с центром кластера.
            offset: [-20, -30]
        }
    ];

    let MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
        '<div style="color: #000000; font-weight: bold;">{{ properties.geoObjects.length }}</div>');

    let clusterMalatia = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 100,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterMalatia.add(malatiaObjects);

    let clusterAjapnyak = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 32,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterAjapnyak.add(ajapnyakObjects);

    let clusterShengavit = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 32,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterShengavit.add(shengavitObjects);

    let clusterNork = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 32,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterNork.add(norkObjects);

    let clusterZeytun = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 32,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterZeytun.add(zeytunObjects);

    let clusterAvan = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 32,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterAvan.add(avanObjects);

    let clusterArabkir = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 32,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterArabkir.add(arabkirObjects);

    let clusterDavtashen = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 32,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterDavtashen.add(davtashenObjects);

    let clusterErebuni = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 32,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterErebuni.add(erebuniObjects);

    let clusterMarash = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 32,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterMarash.add(marashObjects);

    let clusterKentron = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 32,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterKentron.add(kentronObjects);

    let clusterEjmiacin = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 32,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterEjmiacin.add(ejmiacinObjects);

    let clusterArmavir = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 32,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterArmavir.add(armavirObjects);

    let clusterGyumri = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 32,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterGyumri.add(gyumriObjects);

    let clusterVanadzor = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 32,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterVanadzor.add(vanadzorObjects);

    let clusterGavar = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 32,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterGavar.add(gavarObjects);

    let clusterMartuni = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 32,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterMartuni.add(martuniObjects);

    // map.geoObjects.add(clusterMalatia);
    // map.geoObjects.add(clusterAjapnyak);
    // map.geoObjects.add(clusterShengavit);
    // map.geoObjects.add(clusterNork);
    // map.geoObjects.add(clusterZeytun);
    // map.geoObjects.add(clusterAvan);
    // map.geoObjects.add(clusterArabkir);
    // map.geoObjects.add(clusterDavtashen);
    // map.geoObjects.add(clusterErebuni);
    // map.geoObjects.add(clusterMarash);
    // map.geoObjects.add(clusterKentron);
    // map.geoObjects.add(clusterEjmiacin);
    // map.geoObjects.add(clusterArmavir);
    // map.geoObjects.add(clusterGyumri);
    // map.geoObjects.add(clusterVanadzor);
    // map.geoObjects.add(clusterGavar);
    // map.geoObjects.add(clusterMartuni);
    // map.geoObjects.add(clusterArmenia);

    // let clusterArmenia = new ymaps.Clusterer(
    //     {
    //         disableClickZoom: false,
    //         gridSize: 64,
    //         // Если опции для кластеров задаются через кластеризатор,
    //         // необходимо указывать их с префиксами "cluster".
    //         clusterIcons: clusterIcons,
    //         clusterIconContentLayout: MyIconContentLayout
    //     });
    // clusterArmenia.add(
    // kentronObjects,
    // malatiaObjects,
    // ajapnyakObjects,
    // shengavitObjects,
    // norkObjects,
    // zeytunObjects,
    // avanObjects,
    // arabkirObjects,
    // davtashenObjects,
    // erebuniObjects,
    // marashObjects,
    // ejmiacinObjects,
    // armavirObjects,
    // gyumriObjects,
    // vanadzorObjects,
    // gavarObjects,
    // martuniObjects);

    let clusterArmenia = new ymaps.Clusterer(
        {
            disableClickZoom: false,
            gridSize: 64,
            // Если опции для кластеров задаются через кластеризатор,
            // необходимо указывать их с префиксами "cluster".
            clusterIcons: clusterIcons,
            clusterIconContentLayout: MyIconContentLayout
        });
    clusterArmenia.add(malatiaObjects)
    clusterArmenia.add(ajapnyakObjects)
    clusterArmenia.add(arabkirObjects)
    clusterArmenia.add(davtashenObjects)
    clusterArmenia.add(shengavitObjects)
    clusterArmenia.add(kentronObjects)
    clusterArmenia.add(norkObjects)
    clusterArmenia.add(marashObjects)
    clusterArmenia.add(zeytunObjects)
    clusterArmenia.add(avanObjects)
    clusterArmenia.add(erebuniObjects)
    clusterArmenia.add(ejmiacinObjects)
    clusterArmenia.add(armavirObjects)
    clusterArmenia.add(gyumriObjects)
    clusterArmenia.add(vanadzorObjects)
    clusterArmenia.add(gavarObjects)
    clusterArmenia.add(martuniObjects)




    map.events.add('boundschange', function(e){
        if (e.get('newZoom') !== e.get('oldZoom')) {
            console.log(e.get('newZoom'))
        }
        if (e.get('newZoom') <= 12){
            map.geoObjects.removeAll()
            map.geoObjects.add(clusterArmenia)
        }
        else{
            map.geoObjects.removeAll()
            map.geoObjects.add(clusterMalatia);
            map.geoObjects.add(clusterAjapnyak);
            map.geoObjects.add(clusterArabkir)
            map.geoObjects.add(clusterDavtashen)
            map.geoObjects.add(clusterShengavit)
            map.geoObjects.add(clusterKentron)
            map.geoObjects.add(clusterNork)
            map.geoObjects.add(clusterMarash)
            map.geoObjects.add(clusterAvan)
            map.geoObjects.add(clusterZeytun)
            map.geoObjects.add(clusterErebuni)
            map.geoObjects.add(clusterEjmiacin)
            map.geoObjects.add(clusterArmavir)
            map.geoObjects.add(clusterGyumri)
            map.geoObjects.add(clusterVanadzor)
            map.geoObjects.add(clusterGavar)
            map.geoObjects.add(clusterMartuni)
        }
    })
}
